// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "slua_unreal/Public/LuaUserWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLuaUserWidget() {}
// Cross Module References
	SLUA_UNREAL_API UClass* Z_Construct_UClass_ULuaUserWidget_NoRegister();
	SLUA_UNREAL_API UClass* Z_Construct_UClass_ULuaUserWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_slua_unreal();
	SLUA_UNREAL_API UFunction* Z_Construct_UFunction_ULuaUserWidget_CallLuaMember();
	SLUA_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FLuaBPVar();
// End Cross Module References
	void ULuaUserWidget::StaticRegisterNativesULuaUserWidget()
	{
		UClass* Class = ULuaUserWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CallLuaMember", &ULuaUserWidget::execCallLuaMember },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics
	{
		struct LuaUserWidget_eventCallLuaMember_Parms
		{
			FString FunctionName;
			TArray<FLuaBPVar> Args;
			FLuaBPVar ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Args_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Args;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Args_Inner;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FunctionName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LuaUserWidget_eventCallLuaMember_Parms, ReturnValue), Z_Construct_UScriptStruct_FLuaBPVar, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args = { UE4CodeGen_Private::EPropertyClass::Array, "Args", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LuaUserWidget_eventCallLuaMember_Parms, Args), METADATA_PARAMS(Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "Args", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FLuaBPVar, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_FunctionName = { UE4CodeGen_Private::EPropertyClass::Str, "FunctionName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LuaUserWidget_eventCallLuaMember_Parms, FunctionName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_Args_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::NewProp_FunctionName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::Function_MetaDataParams[] = {
		{ "Category", "slua" },
		{ "ModuleRelativePath", "Public/LuaUserWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULuaUserWidget, "CallLuaMember", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(LuaUserWidget_eventCallLuaMember_Parms), Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULuaUserWidget_CallLuaMember()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULuaUserWidget_CallLuaMember_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULuaUserWidget_NoRegister()
	{
		return ULuaUserWidget::StaticClass();
	}
	struct Z_Construct_UClass_ULuaUserWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LuaStateName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LuaStateName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LuaFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LuaFilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULuaUserWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_slua_unreal,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULuaUserWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULuaUserWidget_CallLuaMember, "CallLuaMember" }, // 3941685410
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULuaUserWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LuaUserWidget.h" },
		{ "ModuleRelativePath", "Public/LuaUserWidget.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaStateName_MetaData[] = {
		{ "Category", "slua" },
		{ "ModuleRelativePath", "Public/LuaUserWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaStateName = { UE4CodeGen_Private::EPropertyClass::Str, "LuaStateName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(ULuaUserWidget, LuaStateName), METADATA_PARAMS(Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaStateName_MetaData, ARRAY_COUNT(Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaStateName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaFilePath_MetaData[] = {
		{ "Category", "slua" },
		{ "ModuleRelativePath", "Public/LuaUserWidget.h" },
		{ "ToolTip", "below UPROPERTY and UFUNCTION can't be put to macro LUABASE_BODY\nso copy & paste them" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaFilePath = { UE4CodeGen_Private::EPropertyClass::Str, "LuaFilePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(ULuaUserWidget, LuaFilePath), METADATA_PARAMS(Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaFilePath_MetaData, ARRAY_COUNT(Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaFilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULuaUserWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaStateName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULuaUserWidget_Statics::NewProp_LuaFilePath,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULuaUserWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULuaUserWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULuaUserWidget_Statics::ClassParams = {
		&ULuaUserWidget::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B010A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ULuaUserWidget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ULuaUserWidget_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ULuaUserWidget_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ULuaUserWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULuaUserWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULuaUserWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULuaUserWidget, 1855601203);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULuaUserWidget(Z_Construct_UClass_ULuaUserWidget, &ULuaUserWidget::StaticClass, TEXT("/Script/slua_unreal"), TEXT("ULuaUserWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULuaUserWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
