// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLuaBPVar;
class UObject;
#ifdef SLUA_UNREAL_LuaBlueprintLibrary_generated_h
#error "LuaBlueprintLibrary.generated.h already included, missing '#pragma once' in LuaBlueprintLibrary.h"
#endif
#define SLUA_UNREAL_LuaBlueprintLibrary_generated_h

#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLuaBPVar_Statics; \
	static class UScriptStruct* StaticStruct();


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetObjectFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UObject**)Z_Param__Result=ULuaBlueprintLibrary::GetObjectFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetBoolFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULuaBlueprintLibrary::GetBoolFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULuaBlueprintLibrary::GetStringFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULuaBlueprintLibrary::GetNumberFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIntFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULuaBlueprintLibrary::GetIntFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromObject) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromBool) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromBool(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromNumber) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromNumber(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromInt) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromInt(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCallToLua) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_StateName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CallToLua(Z_Param_FunctionName,Z_Param_StateName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCallToLuaWithArgs) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_GET_PROPERTY(UStrProperty,Z_Param_StateName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CallToLuaWithArgs(Z_Param_FunctionName,Z_Param_Out_Args,Z_Param_StateName); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetObjectFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UObject**)Z_Param__Result=ULuaBlueprintLibrary::GetObjectFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetBoolFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULuaBlueprintLibrary::GetBoolFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetStringFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULuaBlueprintLibrary::GetStringFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULuaBlueprintLibrary::GetNumberFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIntFromVar) \
	{ \
		P_GET_STRUCT(FLuaBPVar,Z_Param_Value); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULuaBlueprintLibrary::GetIntFromVar(Z_Param_Value,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromObject) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromBool) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromBool(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromNumber) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromNumber(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCreateVarFromInt) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CreateVarFromInt(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCallToLua) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_PROPERTY(UStrProperty,Z_Param_StateName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CallToLua(Z_Param_FunctionName,Z_Param_StateName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCallToLuaWithArgs) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_GET_PROPERTY(UStrProperty,Z_Param_StateName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=ULuaBlueprintLibrary::CallToLuaWithArgs(Z_Param_FunctionName,Z_Param_Out_Args,Z_Param_StateName); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULuaBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULuaBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULuaBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ULuaBlueprintLibrary)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_INCLASS \
private: \
	static void StaticRegisterNativesULuaBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULuaBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULuaBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ULuaBlueprintLibrary)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULuaBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULuaBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULuaBlueprintLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULuaBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULuaBlueprintLibrary(ULuaBlueprintLibrary&&); \
	NO_API ULuaBlueprintLibrary(const ULuaBlueprintLibrary&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULuaBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULuaBlueprintLibrary(ULuaBlueprintLibrary&&); \
	NO_API ULuaBlueprintLibrary(const ULuaBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULuaBlueprintLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULuaBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULuaBlueprintLibrary)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_35_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h_38_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LuaBlueprintLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
