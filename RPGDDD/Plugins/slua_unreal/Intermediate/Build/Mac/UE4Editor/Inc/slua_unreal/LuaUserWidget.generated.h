// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLuaBPVar;
#ifdef SLUA_UNREAL_LuaUserWidget_generated_h
#error "LuaUserWidget.generated.h already included, missing '#pragma once' in LuaUserWidget.h"
#endif
#define SLUA_UNREAL_LuaUserWidget_generated_h

#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULuaUserWidget(); \
	friend struct Z_Construct_UClass_ULuaUserWidget_Statics; \
public: \
	DECLARE_CLASS(ULuaUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ULuaUserWidget)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_INCLASS \
private: \
	static void StaticRegisterNativesULuaUserWidget(); \
	friend struct Z_Construct_UClass_ULuaUserWidget_Statics; \
public: \
	DECLARE_CLASS(ULuaUserWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ULuaUserWidget)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULuaUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULuaUserWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULuaUserWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULuaUserWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULuaUserWidget(ULuaUserWidget&&); \
	NO_API ULuaUserWidget(const ULuaUserWidget&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULuaUserWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULuaUserWidget(ULuaUserWidget&&); \
	NO_API ULuaUserWidget(const ULuaUserWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULuaUserWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULuaUserWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULuaUserWidget)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_23_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaUserWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
