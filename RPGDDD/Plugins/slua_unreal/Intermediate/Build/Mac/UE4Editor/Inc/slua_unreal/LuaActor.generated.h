// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLuaBPVar;
#ifdef SLUA_UNREAL_LuaActor_generated_h
#error "LuaActor.generated.h already included, missing '#pragma once' in LuaActor.h"
#endif
#define SLUA_UNREAL_LuaActor_generated_h

#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaActor(); \
	friend struct Z_Construct_UClass_ALuaActor_Statics; \
public: \
	DECLARE_CLASS(ALuaActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaActor)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_INCLASS \
private: \
	static void StaticRegisterNativesALuaActor(); \
	friend struct Z_Construct_UClass_ALuaActor_Statics; \
public: \
	DECLARE_CLASS(ALuaActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaActor)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaActor(ALuaActor&&); \
	NO_API ALuaActor(const ALuaActor&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaActor(ALuaActor&&); \
	NO_API ALuaActor(const ALuaActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaActor)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_49_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaPawn(); \
	friend struct Z_Construct_UClass_ALuaPawn_Statics; \
public: \
	DECLARE_CLASS(ALuaPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaPawn)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_INCLASS \
private: \
	static void StaticRegisterNativesALuaPawn(); \
	friend struct Z_Construct_UClass_ALuaPawn_Statics; \
public: \
	DECLARE_CLASS(ALuaPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaPawn)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaPawn(ALuaPawn&&); \
	NO_API ALuaPawn(const ALuaPawn&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaPawn(ALuaPawn&&); \
	NO_API ALuaPawn(const ALuaPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaPawn); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaPawn)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_77_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaCharacter(); \
	friend struct Z_Construct_UClass_ALuaCharacter_Statics; \
public: \
	DECLARE_CLASS(ALuaCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaCharacter)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_INCLASS \
private: \
	static void StaticRegisterNativesALuaCharacter(); \
	friend struct Z_Construct_UClass_ALuaCharacter_Statics; \
public: \
	DECLARE_CLASS(ALuaCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaCharacter)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaCharacter(ALuaCharacter&&); \
	NO_API ALuaCharacter(const ALuaCharacter&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaCharacter(ALuaCharacter&&); \
	NO_API ALuaCharacter(const ALuaCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaCharacter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaCharacter)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_100_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_102_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaController(); \
	friend struct Z_Construct_UClass_ALuaController_Statics; \
public: \
	DECLARE_CLASS(ALuaController, AController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_INCLASS \
private: \
	static void StaticRegisterNativesALuaController(); \
	friend struct Z_Construct_UClass_ALuaController_Statics; \
public: \
	DECLARE_CLASS(ALuaController, AController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaController(ALuaController&&); \
	NO_API ALuaController(const ALuaController&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaController(ALuaController&&); \
	NO_API ALuaController(const ALuaController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_123_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_125_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaPlayerController(); \
	friend struct Z_Construct_UClass_ALuaPlayerController_Statics; \
public: \
	DECLARE_CLASS(ALuaPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaPlayerController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_INCLASS \
private: \
	static void StaticRegisterNativesALuaPlayerController(); \
	friend struct Z_Construct_UClass_ALuaPlayerController_Statics; \
public: \
	DECLARE_CLASS(ALuaPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaPlayerController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaPlayerController(ALuaPlayerController&&); \
	NO_API ALuaPlayerController(const ALuaPlayerController&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaPlayerController(ALuaPlayerController&&); \
	NO_API ALuaPlayerController(const ALuaPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaPlayerController)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_146_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_148_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaGameModeBase(); \
	friend struct Z_Construct_UClass_ALuaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALuaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaGameModeBase)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_INCLASS \
private: \
	static void StaticRegisterNativesALuaGameModeBase(); \
	friend struct Z_Construct_UClass_ALuaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALuaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaGameModeBase)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaGameModeBase(ALuaGameModeBase&&); \
	NO_API ALuaGameModeBase(const ALuaGameModeBase&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaGameModeBase(ALuaGameModeBase&&); \
	NO_API ALuaGameModeBase(const ALuaGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaGameModeBase)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_169_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_171_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallLuaMember) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_FunctionName); \
		P_GET_TARRAY_REF(FLuaBPVar,Z_Param_Out_Args); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLuaBPVar*)Z_Param__Result=P_THIS->CallLuaMember(Z_Param_FunctionName,Z_Param_Out_Args); \
		P_NATIVE_END; \
	}


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALuaHUD(); \
	friend struct Z_Construct_UClass_ALuaHUD_Statics; \
public: \
	DECLARE_CLASS(ALuaHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaHUD)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_INCLASS \
private: \
	static void StaticRegisterNativesALuaHUD(); \
	friend struct Z_Construct_UClass_ALuaHUD_Statics; \
public: \
	DECLARE_CLASS(ALuaHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/slua_unreal"), NO_API) \
	DECLARE_SERIALIZER(ALuaHUD)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALuaHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaHUD(ALuaHUD&&); \
	NO_API ALuaHUD(const ALuaHUD&); \
public:


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALuaHUD(ALuaHUD&&); \
	NO_API ALuaHUD(const ALuaHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALuaHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALuaHUD); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALuaHUD)


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_PRIVATE_PROPERTY_OFFSET
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_186_PROLOG
#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_RPC_WRAPPERS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_INCLASS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_PRIVATE_PROPERTY_OFFSET \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_RPC_WRAPPERS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_INCLASS_NO_PURE_DECLS \
	RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h_188_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID RPGDDD_Plugins_slua_unreal_Source_slua_unreal_Public_LuaActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
