// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActorSec.generated.h"

UCLASS()
class RPGDDD_API AMyActorSec : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActorSec();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    float     m_fRunningTime;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
