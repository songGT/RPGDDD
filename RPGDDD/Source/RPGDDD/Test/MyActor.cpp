// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"
#include "iostream"
#include "lua/lua.hpp"


// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
    CubeMesh = FindComponentByClass<UStaticMeshComponent>();
    RootComponent = CubeMesh;
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    std::cout << "every tick";
    
    //获取Actor的位置 保存到NewLocation变量
    FVector NewLocation = GetActorLocation();
    //获取时间差值
    float DeltaHeight = (FMath::Sin(m_fRunningTime + DeltaTime) - FMath::Sin(m_fRunningTime));
    //改变的高度，每次递增20.0f
    NewLocation.Z += DeltaHeight * 20.0f;
    //改变运行时间的值
    m_fRunningTime += DeltaTime;
    //设置当前Actor的位置
    SetActorLocation(NewLocation);
    
}

